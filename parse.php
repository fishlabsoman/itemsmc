<html>
<header>

<style type="text/css">

TABLE {
	background: white;
	color: black;
	border-collapse: collapse;
}

TD, TH {
	background: #FFF;
	padding: 4px;

}

.show{
	background: #999;
}

.sub {

	display: none;
}

.haveSub {
	background: #DDD;
}

.hover {
	cursor: pointer;
}
#fixed{
	position: fixed;
	left: 0;
	top: 0;
	width: 100%;
}
IMG {

	padding: 1px;
}
/*костылик для рамочки :3   */
IMG:hover {
	border: 1px solid black;
	padding: 0px;
}
</style>
</header>
<body>

<script type="text/javascript">
//сюдой лучше не соваться, лол ))
//кажется можно было просто насоздавать кастомных параметров для всяких tr'ов и прочих.
// типа value="тест", но hiddenBySUPERJS="aga"
function showClass(value) {
	var newname = "show n"+value;
	var stdFinded = 0;//флажок для обратного скрытия
	var iter = 0;

	//долбаный велосипед
	//там к каждому столбику отдельно применяет. в среднем до 10 итераций даже на 20 итемов
	//если правильно отработает логика (обязана в break уйти), то не понадобится столько раз крутить
	while (iter++ < 300 ) {
		console.log(iter);
		var elems = document.getElementsByClassName(value);
		//если найдено хоть что то
		if (elems.length != 0) {
			stdFinded = 1;
			for (var i = 0; i < elems.length; i++) {
				elems[i].className = newname;
			}
		} else {
			//если не найден класс в принципе с первой попытки, то скорее всего он уже переименован.
			//делаем обратное переименование
			if (stdFinded == 0) {
				stdFinded = 1;//сбрасываем флаг что бы не зациклиться в этом блоке
				newname = "sub "+value;
				value = "show n"+value;
				continue;
			} else {
				//ничего не нашли, или больше ничего не нашли
				break;
			}
		}
	}
}
</script>

<!--ссылки, топ-->
<table border='1' id="fixed">
	<tr>
		<td>
			<a href="parse.php?hide=0">Без скрытых модов</a>
		</td>
		<td>
			<a href="parse.php?hide=10">Cкрыты моды где меньше 10 итемов</a>
		</td>
		<td>
			<a href="parse.php?hide=30">Cкрыты моды где меньше 30 итемов</a>
		</td>
		<td>
			<a href="parse.php?hide=50">Cкрыты моды где меньше 50 итемов</a>
		</td>
		<td>
			<a href="parse.php?hide=100">Cкрыты моды где меньше 100 итемов</a>
		</td>
	</tr>
	<tr>
		<td colspan='5'>
		тут типа элементы управления
		</td>
	</tr>
</table>


<?php

//даёт красивую ссылочку на имажечку
function imgUrl($value) {
	$iurl = str_replace(array(":","/","|"), "_", $value);
	$iurl = str_replace(array("#"),"%23",$iurl);

	return "itempanel_icons/{$iurl}.png";
}

//а этот чистит от спецсимволов что я видел, спешали фор классес
function toPlain($value) {
	return  str_replace(array(":","/","|"), "_", $value);
}




//страшный парсер
$csvHandle = fopen("itempanel.csv", "r");

$csv = array();
while ($csvLine = fgets($csvHandle)) {
	$csv[] = trim($csvLine);
}
$items = array();
$items['mods'] = array();
foreach ($csv as $value) {
	$a = explode(',',$value);

	$mod = explode(":", $a[0])[0];
	$key = $a[0];
	$id_meta = $a[1].':'.$a[2];
	$id = $a[1];
	$meta = $a[2];
	$name = $a[4];
	$iurl = imgUrl($name);
	$sub = 0;
	if (isset($items[$mod][$key])) {
		$sub = 1;
		$items[$mod][$key]['sub'][$meta] = array(
		                                       'id_meta'=>$id_meta,
		                                       'id'=>$id,
		                                       'meta'=>$meta,
		                                       'name'=>$name,
		                                       'key'=>$key,
		                                       'iurl'=>$iurl,
		                                       'mod'=>$mod,
		                                       'parent'=>$items[$mod][$key]['id_meta']
		                                   );
	} else {
		$sub = 0;
		$items[$mod][$key]['id_meta'] = $id_meta;
		$items[$mod][$key]['id'] = $id;
		$items[$mod][$key]['meta'] = $meta;
		$items[$mod][$key]['name'] = $name;
		$items[$mod][$key]['key'] = $key;
		$items[$mod][$key]['iurl'] = $iurl;
		$items[$mod][$key]['mod'] = $mod;
		$items[$mod][$key]['parent'] = 0;
	}


	if (!isset($items['mods'][$mod])) {
		$items['mods'][$mod]['firstElem'] = array('name'=>$items[$mod][$key]['name'], 'id_meta'=>$id_meta);
		$items['mods'][$mod]['countBlocks'] = 1;
		$items['mods'][$mod]['countBlocksWOSubs'] = 1;
	} else {

		$items['mods'][$mod]['countBlocks']++;//такое ощущение что что то дублируется. см BuildCraft|Robotics (мб не работает в имени "|"?)

		if (0 == $sub) //это для удобночитаемости. без вложенности, лол
			$items['mods'][$mod]['countBlocksWOSubs']++;

	}
}
$items['mods']['count'] = count($items['mods']);


$json = json_encode($items);
file_put_contents("items.json", $json);
unset($json);



/*
$items = json_decode(file_get_contents("items.json"),true);
*/
/*
echo "<pre>";
print_r($items);
echo "</pre>";
*/

//sub 0 - !sub, not have sub
//sub 1 - !sub, have sub
//sub 2 - sub
function printTr($value, $sub = 0) {
	$elementClass = "k_".toPlain($value['key']);

	if ($sub == 2) {
		$class = "sub {$elementClass}";
	} else if ($sub == 0) {
		$class = "normal";
	} else if ($sub == 1) {
		$class = "haveSub";
	}

	echo "<tr class=\"{$class}\" id=\"".toPlain($value['id_meta'])."\">";

	{
		if ($sub == 1) {
			echo "<td onclick=\"showClass('{$elementClass}')\" class=\"{$class} hover\">";
		} else {
			echo "<td class=\"{$class}\">";
		}
		echo $value['name'];
		if ($sub == 1) {
			echo "<img  src=\"next.png\">\n";
			echo "Total: ".count($value['sub']);
		}

		echo"</td>\n";
	}


	echo "<td class=\"{$class}\">"."<img  src=\"{$value['iurl']}\"></td>\n";

	echo "<td class=\"{$class}\">". $value['key']."</td>\n";

	echo "<td class=\"{$class}\">". $value['id_meta']."</td>\n";


	//ну тут я планирую записывать весь хлам обратно в жсон:
	//либо задать у каждого итема поле джобс и задать в нем значения типа стоимости, самой работы итд итп
	//либо сделать это отдельно, скажем в $items["jobs"][jobs]['block']=xp_count=>1; coin_count=>255
	//причем сделать логику на субэлементы если есть, а вообще так то надо посмотреть как работают субы в
	//самом плагине, может можно родителю задать фигню и увидеть что то далее в субах
	//вообще как нить бы разобрать по типам... типа блок, тул, машина, росток.
	$checked = false;
	if(isset($jobs["builder"]) || rand(0,3) == 0){//for fun
		$checked = true;
	}


	//долбаный чекбокс, эко его рас**сило
	//вот кстати о чекбоксе. есть 2 варика
	//
	//1:
	//клик по чеку вызывает post/get запрос на обновлятор и он обновляет жсон/джобс конфиг/и проч
	//^а вот может, внезапно лагать, или если не сделать задержки по лок (lock) файлам/пайпам то затирать изменения
	//пока первый тормозит запрос, а вторая галочка прошла.
	//
	//2:
	//или наделать галочек и упдАтать по общей кнопке/раз в 10 секунд собрав все галки с поля и проч проч.
	//^тогда добавить флагу что не менялось или менялось
	//
	echo "	  <td class=\"{$class}\">
				<input type=\"checkbox\"
					id=\"".toPlain($value['id_meta'])."\"
					name=\"myTextEditBox\"
					value=\"checked\" ".
					($checked ? "checked" : "")."
				/>
				тест фигня<!--да тут сейчас какая то ерунда!-->
			  </td>\n";
	echo "</tr>";
	return;
}




echo "<br><br><br>";//это типа накрыто лальным топом ссылками

if(!isset($_GET['mod'])){
	echo "<table border=1>\n";
	foreach ($items['mods'] as $key=>$value){
		if(!isset($value['countBlocks']))continue;//игнорим не моды. у модов есть каунтблокс
		if(isset($_GET['hide']) && $value['countBlocks'] < $_GET['hide'])continue;//ignore small mods
		echo "<tr>\n";
		echo "<td>
			<a href=\"parse.php?mod={$key}\">
				<img src=\"".imgUrl($value['firstElem']['name'])."\">
			</a>
		</td>
		<td>
			Mod: <a href=\"parse.php?mod={$key}\">{$key}</a>
		</td>
		<td>
			Total with sub's:
		</td>
		<td style=\"background-color: #DDF\" align=\"center\">
			{$value['countBlocks']}
		</td>
		<td>
			Total WO subs:
		</td>
		<td style=\"background-color: #Fdd\" align=\"center\">
			{$value['countBlocksWOSubs']}
		</td>
		";

		echo "</tr></a>\n";
	}
	echo "</table>\n";
}


//этот лал надо сделать хорошо
//тут форичем всё выводить
//или ифом выводить только что надо
//нужно подумать как сделать выбор
//пока коментится тот или иной блок


/*
	foreach($items as $key=>$value) {

//*/
	if(isset($_GET["mod"])){
	$key = $_GET["mod"];
	$value = $items[$key];
//*/
	echo "<h1>Mod:".$key."</h1>\n";
	echo "<table border='1' cellspacing='0'>";
	foreach ($value as $key=>$value) {
		if(!isset($value['key']))break;//не имеет имени итема? мимо проходим

		if (isset($value['sub'])) {//если у итема есть дочерние (субы)
			printTr($value, 1);
		} else {
			printTr($value, 0);//в таком случае это обычный итем
		}

		if (isset($value['sub'])) {
			foreach ($value['sub'] as $value) { //выводим суб'ы
				printTr($value, 2);
			}
		}
	}
	echo "</table>";
}

?>
</body>
</html>